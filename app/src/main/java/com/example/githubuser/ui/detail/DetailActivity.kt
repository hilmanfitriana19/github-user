package com.example.githubuser.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.githubuser.R
import com.example.githubuser.User
import com.example.githubuser.databinding.ActivityDetailBinding
import de.hdodenhof.circleimageview.CircleImageView
import org.w3c.dom.Text

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding

    companion object{
        const val EXTRA_USER = "extra_user"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title="User Detail"

        val user = intent.getParcelableExtra<User>(EXTRA_USER) as User

        val username:TextView = findViewById(R.id.tv_dtl_username)
        val name:TextView = findViewById(R.id.tv_dtl_name)
        val company:TextView = findViewById(R.id.tv_dtl_company)
        val location:TextView = findViewById(R.id.tv_dtl_location)
        val repository:TextView = findViewById(R.id.tv_dtl_repository)
        val following:TextView = findViewById(R.id.tv_dtl_following)
        val followers:TextView = findViewById(R.id.tv_dtl_followers)
        val avatar: CircleImageView = findViewById(R.id.img_dtl_item_photo)

        username.text = "@"+user.login
        name.text = user.name
        company.text = user.company
        location.text = user.location
        val repo = getDrawable(R.drawable.repo_icon)
        repo?.setBounds(0,0,70,70)
        repository.setCompoundDrawables(repo, null, null,null)
        repository.compoundDrawablePadding=20
        repository.text = user.repository
        followers.text = user.follower
        following.text = user.following
        avatar.setImageResource(user.avatar_url)



    }
}