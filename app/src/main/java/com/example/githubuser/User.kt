package com.example.githubuser

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User (
    var name:String,
    var login:String,
    var avatar_url:Int,
    var follower:String,
    var following:String,
    var company:String,
//    var followers_list:ArrayList<User>,
//    var following_list:ArrayList<User>,
    var location:String,
    var repository:String

):Parcelable