# Github User App (Submission 1)
Android Application for Dicoding Course **Belajar Fundamental Aplikasi Android** 


## Submission CheckList

- Menampilkan data pada halaman aplikasi dengan minimal jumlah 10 item.
- Menggunakan RecyclerView.
- Menampilkan avatar dan informasi user pada halaman Detail User.
- Menggunakan Parcelable sebagai interface dari obyek data yang akan dikirimkan antar Activity.
- List Item untuk RecyclerView disusun menggunakan ConstraintLayout.


## Demo App

<img src="./demo app/1.jpg" width="250" heigth="500"/>
<img src="./demo app/2.jpg" width="250" heigth="500"/>
<img src="./demo app/3.jpg" width="250" heigth="500"/>
<img src="./demo app/4.jpg" width="250" heigth="500"/>
<img src="./demo app/5.jpg" width="250" heigth="500"/>
<img src="./demo app/6.jpg" width="250" heigth="500"/>
